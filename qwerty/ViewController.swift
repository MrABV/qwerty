//
//  ViewController.swift
//  qwerty
//
//  Created by Anatoliy Bondik on 17.02.17.
//  Copyright © 2017 Anatoliy Bondik. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        hm.larger(value1: 1, value2: 2)
        
        hm.squarecube(value: 10)
        
        print (hm.divisor(numb: 12))
        
        print (hm.perfect(numb: 6))
        
        print("they would have had", hm.manhattan(numb: 2017))
        
        hm.poorStudent(scholarship: 700, expences: 1000)
        
        print ("he can survive for", hm.troubleStudent(scholarship: 700, expences: 1000), "month")
        
        print ( hm.reverseNumbers(number: 5003))
        
        hm.howOldIsElizabet(age: 18)
    }

}

