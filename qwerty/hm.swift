//
//  Homework 2.swift
//  swiftClasses
//
//  Created by Anatoliy Bondik on 16.02.17.
//  Copyright © 2017 Anatoliy Bondik. All rights reserved.
//

import UIKit

class hm: NSObject {
    
    static func larger(value1 : Double ,value2 : Double ) {
        let val1 = value1
        let val2 = value2
        if val1 > val2 {
            print ("val1 is greater")
        }
        else if val1 < val2 {
            print ("val2 is greater")
        }
        else {
            print ("equal")
        }
    }
    
    static func squarecube (value : Double) {
        let square = value * value
        let cube = square * value
        print("square is", square, "cube is", (cube))
    }
    
    static func count(numb: Int) {
        for number in 0..<numb {
            print("number" , number)
        }
        for number in (0..<numb).reversed() {
            print("number", number)
        }
    }
    // return the sequence and count the number of the returning results
    static func divisor(numb: Int) -> Double {
        let number = numb/2
        var dividersCount = 0
        for divider in 1...number {
            let product = Double(number) / Double(divider)
            let integerProduct = Int(product)
            let zero = product - Double(integerProduct)
            if zero == 0 {
                print (Int(product))
                dividersCount += 1
                return product
            }
        }
        print("dividersCount =", dividersCount)
        return 0
    }
    
    // sum up all the returning results
    static func perfect(numb: Int) -> Bool {
        let number = numb
        var dividerSum = 0
        for divider in 2...number {
            let product = Double(number) / Double(divider)
            let integerProduct = Int(product)
            let zero = product - Double(integerProduct)
            if zero == 0 {
                dividerSum += Int(product)
                if dividerSum == number {
                    return true
                }
            }
        }
        return false
    }
    
    static func manhattan (numb: Int) -> Double {
        let years = numb - 1826
        var money: Double = 24
        var counter = 0
        for _ in 0..<years {
            money += 0.06 * money
            var amount = money
            counter += 1
            if counter >= numb - 1826 {
                return amount
            }
        }
        return 0
    }
    
    static func poorStudent (scholarship: Int, expences: Int) {
        let sch = scholarship
        var budget = 0.0
        var exp: Double = Double(expences)
        var month = 0
        for _ in 0..<10 {
            exp += exp * 0.03
            let budg = exp - Double(sch)
            budget += budg
            month += 1
            print("for", month, "month he needs" , budget)
        }
    }
    
    static func troubleStudent (scholarship: Int, expences: Int) -> Int {
        let sch:Double = Double(scholarship)
        var exp: Double = Double(expences)
        var budget = 2400.0
        var month = 0
        for _ in 0..<10{
            budget = budget - exp * 0.03 - sch
            month += 1
            if budget <= 0 {
                return month - 1
            }
        }
        return 0
    }
    
    static func reverseNumbers (number: Int) -> Int {
        var numb = number
        var final = 0
        if number >= 1000 {
        for _ in 0..<10 {
            numb -= 1000
            final += 1
            if  numb <= 1000 {
                for _ in 0..<10 {
                    if numb >= 100 {
                    numb -= 100
                    final += 10
                    }
                    else if numb <= 100 {
                        for _ in 0..<10 {
                            if numb >= 10{
                            numb -= 10
                            final += 100
                            }
                            else if numb <= 10 {
                                for _ in 0..<10 {
                                    if numb >= 0{
                                    numb -= 1
                                    final += 1000
                                    }
                                    else if numb <= 0 {
                                        final -= 1000
                                        return final
                                    }
                                }
                            }
                            }
                        }
                    }
                }
            }
        }
        else if numb >= 100 {
            for _ in 0..<10 {
                if numb >= 100 {
                    numb -= 100
                    final += 1
                }
                else if numb <= 100 {
                    for _ in 0..<10 {
                        if numb >= 10{
                            numb -= 10
                            final += 10
                        }
                        else if numb <= 10 {
                            for _ in 0..<10 {
                                if numb >= 0{
                                    numb -= 1
                                    final += 100
                                }
                                else if numb <= 0 {
                                    final -= 100
                                    return final
                                }
                            }
                        }
                    }
                }
            }
        }
        else if numb >= 10 {
            for _ in 0..<10 {
                if numb >= 10{
                    numb -= 10
                    final += 1
                }
                else if numb <= 10 {
                    for _ in 0..<10 {
                        if numb >= 0{
                            numb -= 1
                            final += 10
                        }
                        else if numb <= 0 {
                            final -= 10
                            return final
                        }
                    }
                }
            }

        }
    return 0
}

    static func howOldIsElizabet (age: Int) {
        if age > 18 {
            print ("too young to drink")
        }
        else {
            print ("time to marry")
        }
    }

}


